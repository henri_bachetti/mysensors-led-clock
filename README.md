# MYSENSORS USB LED CLOCK

The purpose of this page is to explain step by step the realization of a 7-segments LED clock based on ARDUINO NANO, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO NANO
 * a NRF24L01
 * a 4 digits display (common cathode, 0.56")
 * some passive components
 * the board is powered by the USB.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/02/une-horloge-numerique-mysensors-cette.html
