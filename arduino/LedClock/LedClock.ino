
#include <Time.h>
#define MY_RADIO_NRF24

//#define MY_DEBUG
#define MY_RF24_CE_PIN      7
#define MY_RF24_CS_PIN      8

#define LED_CHILD           8

#include <SPI.h>
#include <MySensors.h>  

// Initialize messages for display
MyMessage textMsg(0, V_TEXT);

#define NDIGITS             4

// PORT C cathodes
#define DIGIT_1             0x08
#define DIGIT_2             0x01
#define DIGIT_3             0x02
#define DIGIT_4             0x04
#define DIGIT_MASKC         0x0f

// PORT D B C segments
#define SEGMENT_A           0x00000020
#define SEGMENT_B           0x00000008
#define SEGMENT_C           0x00000040
#define SEGMENT_D           0x00000200
#define SEGMENT_E           0x00000400
#define SEGMENT_F           0x00000010
#define SEGMENT_G           0x00100000
#define SEGMENT_DP          0x00200000
#define SEGMENT_MASK        0x00300678
#define SEGMENT_MASKD       0x78
#define SEGMENT_MASKB       0x06
#define SEGMENT_MASKC       0x30

char diglist[] = {DIGIT_1, DIGIT_2, DIGIT_3, DIGIT_4};

uint32_t segment[] = {
    SEGMENT_A | SEGMENT_B | SEGMENT_C | SEGMENT_D | SEGMENT_E | SEGMENT_F,                // 0
    SEGMENT_B | SEGMENT_C,                                                                // 1
    SEGMENT_A | SEGMENT_B | SEGMENT_G | SEGMENT_D | SEGMENT_E,                            // 2
    SEGMENT_A | SEGMENT_B | SEGMENT_C | SEGMENT_D | SEGMENT_G,                            // 3
    SEGMENT_B | SEGMENT_C | SEGMENT_F | SEGMENT_G,                                        // 4
    SEGMENT_A | SEGMENT_C | SEGMENT_D | SEGMENT_F | SEGMENT_G,                            // 5
    SEGMENT_A | SEGMENT_C | SEGMENT_D | SEGMENT_E | SEGMENT_F | SEGMENT_G,                // 6
    SEGMENT_A | SEGMENT_B | SEGMENT_C,                                                    // 7
    SEGMENT_A | SEGMENT_B | SEGMENT_C | SEGMENT_D | SEGMENT_E | SEGMENT_F | SEGMENT_G,    // 8
    SEGMENT_A | SEGMENT_B | SEGMENT_C | SEGMENT_D | SEGMENT_F | SEGMENT_G,                // 9
    SEGMENT_A | SEGMENT_B | SEGMENT_C | SEGMENT_E | SEGMENT_F | SEGMENT_G,                // A
    SEGMENT_C | SEGMENT_D | SEGMENT_E | SEGMENT_F | SEGMENT_G,                            // b
    SEGMENT_A | SEGMENT_D | SEGMENT_E | SEGMENT_F,                                        // C
    SEGMENT_B | SEGMENT_C | SEGMENT_D | SEGMENT_E | SEGMENT_G,                            // d
    SEGMENT_A | SEGMENT_D | SEGMENT_E | SEGMENT_F | SEGMENT_G,                            // E
    SEGMENT_A | SEGMENT_E | SEGMENT_F | SEGMENT_G,                                        // F
    SEGMENT_D                                                                             // _
};

#define LUMINOSITY_MAX  1000
#define REFRESH_HZ      400
#define REFRESH_DELAY   refreshDelay()
#define FIXED           1
#define BLINK           2
#define FADE_IN         3
#define FADE_OUT        4
#define FADE_INOUT      5
#define BLINK_TIME      2000
#define DIMMING_NSTEP   100
#define DIMMING_STEP    (BLINK_TIME / DIMMING_NSTEP)

uint16_t mode = FIXED;
float luminosity = 100;
float dimmingTable[DIMMING_NSTEP];

uint32_t refreshDelay(void)
{
  float delay = 1000000 / REFRESH_HZ;
  return (uint32_t)delay;
}

void usDelay(uint32_t t)
{
  delay(t / 1000);
  delayMicroseconds(t % 1000);
}

void test(void)
{
  PORTC &= ~DIGIT_MASKC;
  PORTD = SEGMENT_A;
  delay(500);
  PORTD &= ~SEGMENT_MASKD;
  PORTD = SEGMENT_B;
  delay(500);
  PORTD &= ~SEGMENT_MASKD;
  PORTD = SEGMENT_C;
  delay(500);
  PORTD &= ~SEGMENT_MASKD;
  PORTB = SEGMENT_D >> 8;
  delay(500);
  PORTB &= ~SEGMENT_MASKB;
  PORTB = SEGMENT_E >> 8;
  delay(500);
  PORTB &= ~SEGMENT_MASKB;
  PORTD = SEGMENT_F;
  delay(500);
  PORTD &= ~SEGMENT_MASKD;
  PORTC = SEGMENT_G >> 16;
  delay(500);
  PORTC &= ~SEGMENT_MASKC;
  PORTC = SEGMENT_DP >> 16;
  delay(500);
  PORTC &= ~SEGMENT_MASKC;
  PORTC = DIGIT_MASKC;
}

void before()
{
  Serial.begin(115200);
  Serial.print("MYSENSORS USB CLOCK: ");
  DDRD = SEGMENT_MASKD;
  DDRB = SEGMENT_MASKB;
  DDRC = DIGIT_MASKC | SEGMENT_MASKC;
  clearDisplay();
  dimmingTable[0] = 1;
  for (int l = 1 ; l < DIMMING_NSTEP ; l++) {
    dimmingTable[l] = dimmingTable[l-1] * 1.073;
  }
  test();
  Serial.println("OK");
}

void setup()
{
  requestTime();
}

void setDigit(int d, int value)
{
  PORTC &= ~diglist[d];
  PORTD |= segment[value] & SEGMENT_MASKD;
  PORTB |= (segment[value] >> 8) & SEGMENT_MASKB;
  PORTC |= (segment[value] >> 16) & SEGMENT_MASKC;
}

void clearDigit(int d)
{
  PORTC |= diglist[d];
  PORTD &= ~SEGMENT_MASKD;
  PORTB &= ~SEGMENT_MASKB;
  PORTC &= ~SEGMENT_MASKC;
}

void clearDisplay(void)
{
  for (int d = 0 ; d < 4 ; d++) {
    clearDigit(d);
  }
}

int displayColon(int colon)
{
  if (colon == FIXED) {
    return 1;
  }
  else {
    if (mode == FADE_IN) {
      if (millis() % BLINK_TIME > BLINK_TIME / 2) {
        PORTC |= SEGMENT_DP >> 16;
        return 1;
      }
    }
    else {
      if ((millis() % BLINK_TIME) < BLINK_TIME / 2) {
        PORTC |= SEGMENT_DP >> 16;
        return 1;
      }
    }
  }
  return 0;
}

void writeDigit(int d, int value, int col=0, int dot=0)
{
  if (luminosity == 0) {
    return;
  }
  setDigit(d, value);
  if (d < 2 && col) {
    displayColon(col);
  }
  uint32_t t = REFRESH_DELAY/LUMINOSITY_MAX*luminosity;
  usDelay((uint16_t)t);
  clearDigit(d);
  t = REFRESH_DELAY-t;
  usDelay(t);
}

uint8_t toDigit(uint8_t c)
{
  if (c >= '0' && c <= '9') {
    return c - '0';
  }
  if (tolower(c) >= 'a' && tolower(c) <= 'f') {
    return c - 'a' + 10;
  }
  return 16; // overflow
}

void displayTime(char *buf, int col)
{
  writeDigit(3, toDigit(buf[0]), col);
  writeDigit(2, toDigit(buf[1]), col);
  writeDigit(1, toDigit(buf[2]), col);
  writeDigit(0, toDigit(buf[3]), col);
}

void presentation() {
  Serial.print("Presentation ");
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("MYSENSORS Clock", "1.1");
  present(LED_CHILD, S_INFO, "Digital Clock");
  send(textMsg.setSensor(LED_CHILD).set("00:00"));
  Serial.println("OK");
}

static boolean timeReceived = false;

void loop()
{
  static unsigned long lastTimeUpdate=0, lastRequest=0, lastTempUpdate=0;
  static char buf[] = "1234";
  static uint16_t lum;

  unsigned long t = millis();
  if ((!timeReceived && t-lastRequest > (unsigned long)10*1000) || (timeReceived && t-lastRequest > (unsigned long)60*60*1000)) {
    // Request time from controller.
    Serial.println("requesting time");
    requestTime();
    timeReceived = false;
    lastRequest = t;
  }
  if (mode == BLINK) {
    luminosity = (millis() % BLINK_TIME) < BLINK_TIME / 2 ? LUMINOSITY_MAX : 0;
  }
  else if (mode == FADE_IN) {
    int dimmingStep = millis() % BLINK_TIME / DIMMING_STEP;
    luminosity = dimmingTable[dimmingStep];
  }
  else if (mode == FADE_OUT) {
    int dimmingStep = millis() % BLINK_TIME / DIMMING_STEP;
    luminosity = dimmingTable[DIMMING_NSTEP-dimmingStep-1];
  }
  else if (mode == FADE_INOUT) {
    int dimmingStep = millis() % BLINK_TIME * 2 / DIMMING_STEP;
    if (dimmingStep < DIMMING_NSTEP) {
      luminosity = dimmingTable[dimmingStep];
    }
    else {
      luminosity = dimmingTable[DIMMING_NSTEP-(dimmingStep-DIMMING_NSTEP)-1];
    }
  }
  int h = hour();
  int m = minute();
  sprintf(buf, "%02d%02d", h, m);
  displayTime(buf, BLINK);
  static unsigned long lastTransmission;
  if (second() == 0 && lastTransmission != h * 60 + m) {
    Serial.println("transmit time to controller");
    transmitTime(h, m);
    lastTransmission = h * 60 + m;
  }
}

// This is called when a new time value was received
void receiveTime(unsigned long controllerTime)
{
  Serial.print("Time value received: ");
  Serial.println(controllerTime);
  setTime(controllerTime);              // time from controller
  timeReceived = true;
}

void transmitTime(int h, int m)
{
  char buf[6];

  sprintf(buf, "%02d:%02d", h, m);
  send(textMsg.setSensor(LED_CHILD).set(buf));
}

